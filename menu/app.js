const btn = document.querySelector("#button");
const btnElement = document.querySelectorAll("span");
const nav = document.querySelector("nav");

const addActive=()=>{
    document.querySelector("main").classList.toggle("active");
    btn.classList.toggle("active")
    btnElement.forEach(span => span.classList.toggle("active"));
    nav.classList.toggle("active")
}
btn.addEventListener("click", addActive);