const data = {
    users: [
        {
            id: 1,
            name: "Karol",
            age: 24,
            nickname: "turtle",
            sex: "male"
        }, {
            id: 2,
            name: "Zosia",
            age: 34,
            nickname: "treefriend",
            sex: "female"
        }, {
            id: 3,
            name: "Krysia",
            age: 16,
            nickname: "lola",
            sex: "female"
        }, {
            id: 4,
            name: "Antek",
            age: 18,
            nickname: "fiolek",
            sex: "male"
        }, {
            id: 5,
            name: "Katastrofix",
            age: 49,
            nickname: "Cpun",
            sex: "male"
        }, {
            id: 6,
            name: "Karolina",
            age: 30,
            nickname: "niała",
            sex: "female"
        }
    ]
}

const User = (props) => {
    let userData = props.data;

    return (
        <>
            <h1>Name: {userData.name}</h1>
            <p>Age: {userData.age}</p>
            <p>Nick: {userData.nickname}</p>
        </>
    )

}


class Api extends React.Component {

    state = {
        select: "all",
    }

    showList = () => {
        let users = data.users;
        switch (this.state.select) {

            case "all":
                return (
                    <>
                        {users.map(user => <User data={user} key={user.id} />)}
                    </>
                )
            case "male":
                users = users.filter(user => user.sex === "male");
                return (
                    <>
                        {users.map(user => <User data={user} key={user.id} />)}
                    </>
                )
            case "female":
                users = users.filter(user => user.sex === "female");
                return (
                    <>
                        {users.map(user => <User data={user} key={user.id} />)}
                    </>
                )
        }
    }
    handleUserSelect(option) {
        this.setState({
            select: option,
        })
    }
    render = () => {

        return (
            <>
                <button onClick={this.handleUserSelect.bind(this, "male")}>Men</button>
                <button onClick={this.handleUserSelect.bind(this, "female")}>Woman</button>
                <button onClick={this.handleUserSelect.bind(this, "all")}>All</button>
                {this.showList()}
            </>
        )
    }
}


ReactDOM.render(<Api data={data.users} />, document.getElementById("root"))