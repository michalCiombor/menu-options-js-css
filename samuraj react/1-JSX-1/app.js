console.log(React);
console.log(ReactDOM);

const element = <div id="pierwszy">Pierwszy element React</div>;

const element2 = React.createElement(
  "div",
  null,
  "Pierwszy element React"
)

const element3 = (
  <div>
    <p id="main" className="red">Tekst</p>
  </div>
)

const element4 = <div><p>Tekst</p></div>

// Wersja skrócona React.Fragment (<>) nie zadziała dla wersji babel mniejszej niż 7.
class Element5 extends React.Component {
  render() {
    return (
      <>
        <section>
          <header>
            <h1>WITAJ NA SWOJEJ PIERWSZEJ STRONIE REACT</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut est obcaecati officiis fugit nemo optio provident ratione, facilis quasi id distinctio expedita veritatis ad ducimus esse commodi veniam omnis! Debitis!</p>
            <input type="text" placeholder="cos dodane na stronie"></input>
          </header>
        </section>
        <section></section>
      </>
    )

  }
}

const element6 = React.createElement(
  "div",
  { id: "nowy", key: "nowy2" },
  "następny element reacta"
)
class Button extends React.Component {
  state = {

    text: "",

  }
  handleClick() {
    const letter = "a"
    this.setState({
      text: this.state.text + letter,
    })
  }

  render() {
    return (
      <>
        <button onClick={this.handleClick.bind(this)}>DODAJ A!</button>
        <h1>{this.state.text}</h1>
      </>
    )
  }
}
class Button2 extends React.Component {
  state = {
    tablica: "",
  }
  handleSubmit() {
    const task = document.querySelector(".ToDoInput");
    const h1 = document.querySelectorAll("h1");
    h1.forEach(el => el.style.fontSize = "50px");
    h1.forEach(el => el.style.fontFamily = "Arial");


    this.setState({
      tablica: this.state.tablica = task.value.toUpperCase()
    })

  }
  render() {
    return (
      <>

        <input onChange={this.handleSubmit.bind(this)} placeholder="Dodaj zadanie" className="ToDoInput"></input>


        <h1>{this.state.tablica}</h1>
      </>
    )

  }
}

class App extends React.Component {

  render() {

    return (
      <>
        <Element5 />
        <div>
          <h1>pierwszy element</h1>
        </div>
        <Button />
        <Button2 />
        <App22 />
        <App23 />
      </>
    )
  }
}
const App22 = () => {
  return (
    <>
      <div>jestem pierwszym elementem z powtórki</div>
    </>
  )
}
class App23 extends React.Component {

  state = {
    time: 0,
    morning: true,
  }

  setHour() {
    const hour = Date.now()
    const newHour = 0
    this.setState({
      time: (hour) / 10000000,
      morning: this.state.morning + newHour
    })

  }
  render() {
    return (
      <>
        <button onClick={this.setHour.bind(this)}>Pobierz czas w ms</button>
        <h1>{this.state.time}</h1>
      </>
    )
  }

}




ReactDOM.render(<App />, document.getElementById("root"));
