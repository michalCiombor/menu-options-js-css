const Cash = (props) => {
    return (
        <React.Fragment>
            <div>{props.name}{props.value}</div>
        </React.Fragment>

    )
}


class App extends React.Component {
    state = {
        value: "",
        product: "electricity"

    }
    static defaultProps = {
        currencies: [
            {
                id: 0,
                name: "złoty",
                ratio: 1,
                title: "Wartość w złotówkach: "
            }, {
                id: 1,
                name: "euro",
                ratio: 4.23,
                title: "Wartość w Euro: "
            }, {
                id: 2,
                name: "dollar",
                ratio: 3.46,
                title: "Wartość w Dolarach: "
            }, {
                name: "pound",
                ratio: 5.13,
                title: "Wartość w Funtach: ",
                id: 3
            }
        ],
        prices: {
            electricity: .51,
            gas: 4.76,
            oranges: 3.79
        }
    }

    handleValueChange = (e) => {
        this.setState({
            value: e.target.value,
        })
    }
    handleSelectChange = (e) => {
        this.setState({
            product: e.target.value,
        })
    }
    setSuffix = (select) => {
        if (select === "electricity") {
            return <em>KWh</em>
        }
        else if (select === "gas") {
            return <em>Litrów</em>
        }
        else if (select === "oranges") {
            return <em>Kilogramów</em>
        }
        else return null;
    }
    setPrice(select) {
        if (select === "electricity") {
            return this.props.prices[select];
        }
        else if (select === "gas") {
            return this.props.prices[select];
        }
        else if (select === "oranges") {
            return this.props.prices[select];
        }
        else return null;

        // this.props.prices[select];
    }

    render() {
        let price = this.setPrice(this.state.product);
        let calculators = this.props.currencies.map(el => <Cash name={el.title} key={el.id} value={this.state.value === "" ? "" : (this.state.value / el.ratio * price).toFixed(2)} />)

        return (
            <div className="calculator">
            <h1>Kalkulator wartości!</h1>
            <label>Wybierz produkt
                
                <select onChange={this.handleSelectChange}>
                    <option value="electricity">prąd</option>
                    <option value="gas">paliwo</option>
                    <option value="oranges">pomarańcze</option>
                </select>
            </label>

                <br />
                <label>Podaj ilość:
                    <input type="number" value={this.state.value} onChange={this.handleValueChange} />{this.setSuffix(this.state.product)}<br /> {`(Cena za jednostkę: ${this.setPrice(this.state.product)})`}
                </label>
                {/* {currencies.map(currency => <li key={currencies.id}> <Cash name={currencies.currency.name} value={currencies.ratio} /></li>)} */}
                {calculators}

            </div >
        )
    }
}
ReactDOM.render(<App />, document.getElementById("root"))