const Person = (props) => {
    return (
        <React.Fragment>
            {props.name},<button onClick={props.onClick}>USUŃ</button>
        </React.Fragment>
    )
}

class List extends React.Component {
    state = {
        index: 0,
        value: "",
        names: [],
    }
    handleInputChange = (e) => {
        this.setState({
            value: e.target.value,
        })
    }
    handleClick = (e) => {
        const person = {
            name: this.state.value,
            id: this.state.index,
        }
        this.setState({
            names: this.state.names.concat(person),
            index: this.state.index + 1,
            value: ""
        })
    }
    handleRemove(id) {
        // console.log(id);
        const names = [...this.state.names];
        const index = names.findIndex(el => el.id === id);
        names.splice(index, 1);
        this.setState({
            names
        })



    }
    render() {
        let people = this.state.names.map(person => <li key={person.id}><Person onClick={this.handleRemove.bind(this, person.id)} name={person.name} /></li>);
        return (
            <div>
                <input type="text" value={this.state.value} onChange={this.handleInputChange} />
                <button onClick={this.handleClick.bind(this, name)}>DODAJ</button>
                <ul>{people} </ul>
            </div>
        )
    }
}
ReactDOM.render(<List />, document.getElementById("root"))