import React, { Component } from 'react';
import Word from './Word'
import './App.css';

class App extends Component {
  state = {
    words: [],
    isLoaded: false,
  }

  componentDidMount() {
    setTimeout(this.fetchData, 3000);

  }
  fetchData = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => this.setState({
        words: json,
        isLoaded: true,
      })
      )
  }


  render() {

    const words = this.state.words.map(word => (<Word key={word.id} name={word.name} login={word.username} street={word.address.street} />));
    return (

      <ul>
        {this.state.isLoaded ? words : "Wczytuję dane!"}
      </ul>

    );
  }
}

export default App;
