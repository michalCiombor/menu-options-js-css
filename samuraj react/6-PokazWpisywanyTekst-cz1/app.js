class App extends React.Component {

  state = {
    value: "",
  }
  handleValueChange = (e) => {

    this.setState({
      value: e.target.value,
    })


  }
  render() {

    return (
      <>

        <input onChange={this.handleValueChange} value={this.state.value} type="text" />
        <h2>{this.state.value}</h2>

      </>
    )

  }
}

ReactDOM.render(<App />, document.getElementById('root'))