const Button = (props) => {
    return (
        <button onClick={props.onClick}>{props.name}</button>
    )
}
class App extends React.Component {
    state = {

        wrozby: ["wrozba 1", "wrozba 2", "wrozba 3"],
        wrozba: "",
        dodana: [],
    }
    handleClickShow = () => {
        let index = Math.floor(Math.random() * this.state.wrozby.length);
        this.setState({

            wrozba: this.state.wrozby[index],
        })

    }
    handleChangeAdd = (e) => {
        this.setState({
            dodana: [e.target.value],
        })

    }
    handleClickAdd = () => {
        this.setState({

            wrozby: this.state.wrozby.concat(this.state.dodana),
            dodana: [],
        })
    }
    render() {

        return (
            <React.Fragment>
                <Button name="Zobacz wróżbę" onClick={this.handleClickShow} /><br />
                <input value={this.state.dodana} type="text" onChange={this.handleChangeAdd} /><Button name="Dodaj wróżbę" onClick={this.handleClickAdd} />
                <br />
                {this.state.wrozba ? <h2>{this.state.wrozba}</h2> : `tutaj pojawi się Twoja wróżba`}
            </React.Fragment>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("root"))