const Item = (props) => {
    const item = props.name;

    return (
        <li style={props.active ? { fontWeight: 'bold' } : { color: "gray" }}
            onClick={props.changeStatus.bind(this, props.id)}> {item}</li >
    )
}