const Header = (props) => {
    const activeItems = props.items.filter(item => item.active === true)
    const number = activeItems.length;


    return (



        <header>
            <h2>Wielkość zamówienia: {number}</h2>

            <h2>Koszt zamówienia: {number ? `${number * 10} złotych` : 'Nie kupujesz to nie płacisz'}</h2>

        </header>
    )
}

