class App extends React.Component {
    state = {
        items: [
            { id: 1, title: "herbata", active: true },
            { id: 2, title: "ziemniaki", active: false },
            { id: 3, title: "kasza", active: false },
            { id: 4, title: "zupa wodna", active: true },
            { id: 5, title: "wrzątek", active: false },
            { id: 6, title: "chleb", active: false },
        ]
    }
    handleChanngeStatus = (id) => {
        console.log(id);
        const items = this.state.items.map(item => {
            if (id === item.id) {
                item.active = !item.active
            }
            return item
        })

        this.setState({
            items: items,
        })
    }
    render() {
        return (
            <>
                <Header items={this.state.items} />
                <ListItems items={this.state.items} changeStatus={this.handleChanngeStatus} />
            </>
        )
    }
}