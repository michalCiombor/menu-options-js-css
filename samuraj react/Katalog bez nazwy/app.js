const Pierwszy = (props) => {
    return (

        <div>
            <h1>Pierwszy {props.name} funkcyjny/bezstanowy React przekazany props to {props.example} a losowa liczba to {props.losowa}</h1>
        </div>

    )

}
class Button extends React.Component {
    state = {
        litera: "",
    }
    handleClick() {

        const index = Math.floor(Math.random() * 20)
        const array = "dqweatyuioplkmnjcbvzc"
        const arrayLiter = [...array]


        this.setState({
            litera: this.state.litera += arrayLiter[index]
        })


    }

    render() {
        return (
            <>
                <button onClick={this.handleClick.bind(this)}>Dodaj literę losową literę do textu</button>
                <h1>Twoja losowa litera to: {this.state.litera}</h1>
            </>
        )
    }
}

class App extends React.Component {
    state = {
        time: 7,
        losowa: Math.floor(Math.random() * 20)
    }
    render() {
        return (
            <>
                <h1>Element klasowy/stanowy</h1>
                < Pierwszy name={this.state.time} example={3 + 9} losowa={this.state.losowa} />
                <Button />

            </>)
    }

}



ReactDOM.render(<App />, document.getElementById("root"))