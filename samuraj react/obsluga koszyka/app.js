const Button = (props) => {


    return (
        <button onClick={props.click} disabled={props.disabled}>{props.text}</button>
    )
}


const AddToCart = (props) => {
    return (
        <React.Fragment>
            <Button click={props.clickSubstring} text="-" action="substract" disabled={props.state <= 0 ? true : false} /><span>{props.state}</span><Button click={props.clickAdd} text="+" action="add" disabled={props.available > 0 ? false : true} />
            {props.state > 0 && <Button text={"KUP"} click={props.sell} />}
        </React.Fragment>
    )

}

class ShoppingCart extends React.Component {
    state = {
        isAvailable: 10,
        isAdded: 0
    }
    handleStateChangeAdd = () => {
        this.setState({
            isAdded: this.state.isAdded + 1,
            isAvailable: this.state.isAvailable - 1
        })
    }
    handleStateChangeSubstring = () => {
        this.setState({
            isAdded: this.state.isAdded - 1,
            isAvailable: this.state.isAvailable + 1
        })
    }
    handleStateSell = () => {
        this.setState({
            isAdded: 0,
            isAvailable: this.state.isAvailable,
        })
    }

    render() {
        const { isAvailable, isAdded } = this.state

        return (
            <>
                <div>Dostępne produkty:{isAvailable}</div>
                <div>Dodane do koszyka:{isAdded}</div>
                <AddToCart clickAdd={this.handleStateChangeAdd} clickSubstring={this.handleStateChangeSubstring} state={this.state.isAdded} available={this.state.isAvailable} sell={this.handleStateSell} />
                <ShoppingCart2 />
            </>
        )
    }
}

class ShoppingCart2 extends React.Component {
    state = {
        isAvailable: 10,
        isAdded: 0
    }
    handleStateChangeAdd = () => {
        this.setState({
            isAdded: this.state.isAdded + 1,
            isAvailable: this.state.isAvailable - 1
        })
    }
    handleStateChangeSubstring = () => {
        this.setState({
            isAdded: this.state.isAdded - 1,
            isAvailable: this.state.isAvailable + 1
        })
    }
    handleStateSell = () => {
        this.setState({
            isAdded: 0,
            isAvailable: this.state.isAvailable,
        })
    }

    render() {
        const { isAvailable, isAdded } = this.state

        return (
            <>
                <div>Dostępne produkty:{isAvailable}</div>
                <div>Dodane do koszyka:{isAdded}</div>
                <AddToCart clickAdd={this.handleStateChangeAdd} clickSubstring={this.handleStateChangeSubstring} state={this.state.isAdded} available={this.state.isAvailable} sell={this.handleStateSell} />

            </>
        )
    }
}
ReactDOM.render(<ShoppingCart />, document.getElementById("root"))