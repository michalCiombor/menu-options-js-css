class App extends React.Component {
    state = {
        show: true,
    }
    handleClick = () => {
        this.setState({
            show: !this.state.show,
        })
    }
    render() {
        return (<React.Fragment>
            <Clock active={this.state.show} />
            <button onClick={this.handleClick}>{this.state.show ? "UKRYJ" : "POKAŻ"}</button>
        </React.Fragment>
        )
    }
}
class Clock extends React.Component {

    state = {

        time: this.getCurrenTime(),
    }
    getCurrenTime() {
        const currentTime = new Date();

        return (
            {
                hours: currentTime.getHours(),
                minutes: currentTime.getMinutes(),
                seconds: currentTime.getSeconds(),
            }
        )
    }
    setTime = () => {
        const time = this.getCurrenTime();
        this.setState({
            time
        })

    }
    componentDidMount() {
        const interval = setInterval(() => this.setTime(), 1000);
    }

    componentWillUnmount() {
        console.log("Komponent odłączony")
    }


    render() {
        const { hours, minutes, seconds } = this.state.time;
        if (this.props.active) {
            return (
                <div>
                    {hours}:{minutes}:{seconds}
                </div>
            )

        } else { return null }
    }
}

ReactDOM.render(<App />, document.getElementById("root"))