const Napis = (props) => {
  const { text } = props;
  return (
    <>
      <h2>{text}</h2>
    </>
  )
}
const SellTitle = (props) => {
  const { text } = props
  return (
    <h1>{text}</h1>
  )
}

class Message extends React.Component {
  state = {
    isValidated: false,
    isConfirmed: false,
  }
  handleChange = () => {
    this.setState({
      isValidated: !this.state.isValidated,
    })
  }
  handleClick = (e) => {
    // debugger
    e.preventDefault();

    this.setState({
      isConfirmed: true,
    })

  }
  DisplayMessage = () => {
    if (this.state.isConfirmed) {
      if (this.state.isValidated) {
        return (
          <Napis text="Możesz kupić bilet" />
        )
      } else {
        return (<Napis text="Nie możesz kupić biletu!!" />
        )
      }
    }
    else {
      return null
    }
  }

  render() {
    return (
      <>
        <form>
          <SellTitle text="Kup bilet na najnowszy horror!!" />
          <input onChange={this.handleChange} type="checkbox" id="validate" />
          <label htmlFor="validate">Mam ukończone 18 lat</label>
          {/* <Napis text="Możesz kupić bilet" /> */}
          <br />

          <button type="submit" onClick={this.handleClick}>Wyślij</button>
          {this.DisplayMessage()}
        </form>
      </>
    )
  }
}

ReactDOM.render(<Message />, document.getElementById("root"));
// <p>{this.state.messageIsActive && text}</p>