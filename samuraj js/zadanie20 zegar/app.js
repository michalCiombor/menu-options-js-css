const clock = () => {
    const data = new Date();
    const seconds = data.getSeconds();
    const minutes = data.getMinutes();
    const hours = data.getHours();

    document.querySelector(".clock span").innerText = `${hours>=10?hours:"0"+hours}/${minutes>=10?minutes:"0"+minutes}/${seconds>=10?seconds:"0"+seconds}`;
}
setInterval(clock, 1000);