let mainIterator = 0;
// let secondaryIterator = 0;

document.querySelector("button").addEventListener("click", () => {
    // secondaryIterator++;
    mainIterator += 2;
    let li = document.createElement("li");
    if (mainIterator % 3 != 0) {
        li.classList.add("font1");
    } else {
        li.classList.add("font2");
        // secondaryIterator = 0;
    }

    li.textContent = `tutaj mamy li z numerem ${mainIterator}`;
    document.querySelector("ul").appendChild(li);
    return true;
});