const input1 = document.createElement("input");
input1.setAttribute("class", "input1");
input1.setAttribute("placeholder", "minimum");
const input2 = document.createElement("input");
input2.setAttribute("class", "input2");
input2.setAttribute("placeholder", "maximum");
const btn = document.createElement("button");
btn.innerText = "wylosuj liczbę";
document.body.appendChild(input1);
document.body.appendChild(input2);
document.body.appendChild(btn);
const div = document.createElement('div');
pokazLosowaLiczbe = () => {
    div.innerText = "";
    const input1 = document.querySelector(".input1");
    const input2 = document.querySelector(".input2");
    let min = input1.value;
    let max = input2.value;
    let losowa = Math.floor(Math.random() * ((max - min + 1) + min));

    div.setAttribute("class", "divNapis");
    div.innerText = `Twoja wylosowana liczba z zakresu od ${min} do ${max} to ${losowa<min?"kliknij jeszcze raz":losowa}`;
    document.body.appendChild(div);

}
btn.addEventListener("click", pokazLosowaLiczbe);