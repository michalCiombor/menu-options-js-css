const spnTxt = document.querySelector(".napis span");
//parametry
const txt = [`Michał Ciombor - Front End Developer`, `drugi text`, 'trzeci text'];
let speed = 100;
let activeLetter = 0;
let activeText = 0;

const addLetter = () => {
    spnTxt.textContent += txt[activeText][activeLetter];
    activeLetter++;
    if (activeLetter === txt[activeText].length) {
        // spnTxt.textContent = "";
        activeText++;
        if (activeText === txt.length) {
            return
        }
        return setTimeout(() => {
            activeLetter = 0;
            spnTxt.textContent = '';
            addLetter()
        }, 2000)

    }

    setTimeout(addLetter, 100)


}
addLetter();

const cursorAnimation = () => {
    document.querySelector("span.cursor").classList.toggle("active");
}
// const indexTyping = setInterval(addLetter, speed);
setInterval(cursorAnimation, 400);