let addSecond = (start = 0) => {
    let time = start;
    return function () {
        time++;
        document.body.innerText = `Jesteś na stronie już ${time} sekund`;
    }

}

let counter = addSecond();
const intervalIndex = setInterval(counter, 1000);

// let setIntervalIndex = setInterval(counter, 1000);

// window.addEventListener("click", counter);