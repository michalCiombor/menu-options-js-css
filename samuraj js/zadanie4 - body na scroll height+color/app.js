document.body.style.height = "1000vh";
let change = true;
let colors = ["red", "green"];
let div = document.createElement("div");
let divHeight = 40;
div.style.position = "fixed";
div.style.top = 0;
div.style.left = 0;
div.style.width = "100vw";
div.style.height = `${divHeight}px`;


document.body.appendChild(div);

function changeHeight() {
    if (change == true) {
        divHeight += 5;
        div.style.backgroundColor = `${colors[0]}`;

    } else {
        divHeight -= 5;
        div.style.backgroundColor = `${colors[1]}`;
    }
    div.style.height = `${divHeight}px`;

    if (divHeight > window.innerHeight * 0.3) {
        change = !change;
    } else if (divHeight == 40) {

        change = !change;
    }
}

window.addEventListener("scroll", changeHeight);