// adding btn to html main file

const btn = document.createElement("button");
btn.style.position = "absolute";
btn.style.top = "10vh";
btn.style.left = "50%";
btn.style.transform = "translateX(-50%)";
btn.style.textTransform = "uppercase";
btn.style.fontSize = "30px";
btn.style.padding = "20px 60px";
btn.textContent = "Wylosuj imię";
btn.addEventListener('mouseover', () => {
    btn.style.cursor = "pointer"
});
document.body.style.textAlign = "center";
document.body.appendChild(btn);

// defying names in array randomNames

let randomNames = ['ania', 'tomek', 'ola', 'krzysiek', 'magda', 'jantoni', 'jeremi', 'dzesika', 'alan', 'brajan', 'tomee lee dzołns'];
let prefixs = ["Wydaje mi się , że", "Jestem pewien, że", "hmmmmmmmmmmmm, myślę że, "]

// changing firts letter to capitalize

let newRandomNames = randomNames.map(element => element.charAt(0).toUpperCase() + element.slice(1));
// adding a new name to array
const insertName = document.createElement("input");
insertName.setAttribute("placeholder", "Dodaj własne imię do bazy");
insertName.setAttribute("id", "name");
const sendName = document.createElement("input");
sendName.setAttribute("type", "button");
sendName.setAttribute("value", "dodaj");
document.body.appendChild(insertName);
document.body.appendChild(sendName);

sendName.addEventListener("click", (e) => {
    let addedName = insertName.value;

    addedName.toUpperCase();
    newRandomNames.push(addedName);
    console.log(addedName);
    console.log("name added");
    insertName.value = "";
})
window.addEventListener("keyup", (e) => {
    if (e.keyCode === 13) {
        let addedName = insertName.value;
        newRandomNames.push(addedName);
        console.log("name added");
        insertName.value = "";
    }
})

// defing html element for msg content (div msg)

const div = document.createElement("div");
div.style.textAlign = "center";
div.textContent = "Naciśnij przycisk, aby wylosować imię";
div.style.letterSpacing = "1px";
div.style.color = "cadetblue";
div.style.fontSize = "15px";
div.style.position = "absolute";
div.style.top = "35vh";
div.style.width = "100%"
document.body.appendChild(div);

// addig random nam to textContent of element
clear = () => {
    div.textContent = "Naciśnij przycisk, aby wylosować imię";

}
drawName = () => {
    let randomName = newRandomNames[Math.floor(Math.random() * newRandomNames.length)];
    div.textContent = `${prefixs[Math.floor(Math.random()*prefixs.length)]} najlepszym imieniem dla Twojego dziecka będzie ${randomName}`;

}
btn.addEventListener('click', drawName);