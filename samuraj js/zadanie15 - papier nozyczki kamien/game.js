// Zobacz gotowy projekt: https://websamuraj.pl/examples/js/projekt7/

const gameSummary = {
    numbers: 0,
    wins: 0,
    losses: 0,
    draws: 0
}

let numberOfGames = 0;
let numberOfWins = 0;
let numberOfLoses = 0;
let numberOfDraws = 0;
let napis = "";

const game = {
    playerHand: "",
    aiHand: "",

}


const hands = [...document.querySelectorAll('.select img')];

//Pierwsza funkcja
//z wykorzystaniem thi(tylko w funkcji zdefiniowanej przez function. Fat arrow dziedziczy this z rodzica - trzeba użyć e.target lub e.currentTarget)

//wybór komputera
function aiChoice() {
    return hands[Math.floor(Math.random() * 3)].dataset.option;
}
//wybór użytkownika
function handSelection() {
    game.playerHand = this.dataset.option;
    console.log(game.playerHand);
    hands.forEach(hand => hand.style.boxShadow = "");
    this.style.boxShadow = "0 0 0 4px yellow";
}

//sprawdzenie wartosci użytkownika i ai

function checkResult(player, ai) {
    if (player === ai) {
        return `draw`
    } else if (player === "kamień" && ai === "nożyczki") {
        return `win`
    } else if (player === "nożyczki" && ai === "papier") {
        return `win`
    } else if (player === "papaier" && ai === "kamień") {
        return `win`
    } else return `loss`

}
//publikacja wyniku

function publishResult(a) {



    if (a == "win") {
        ++numberOfGames;
        ++numberOfWins;
        napis = "wygrałeś :)"

    } else if (a == "draw") {
        ++numberOfGames;
        ++numberOfDraws;
        napis = "remis!"


    } else {
        ++numberOfGames;
        ++numberOfLoses;
        napis = "przegrałeś :("
    }
    //podmiana wartości napisów
    document.querySelector('[data-summary="your-choice"]').innerText = game.playerHand;
    document.querySelector(`[data-summary="ai-choice"]`).innerText = game.aiHand;
    document.querySelector(`[data-summary="who-win"]`).innerText = napis;
    document.querySelector(".numbers span").innerText = numberOfGames;
    document.querySelector(".wins span").innerText = numberOfWins;
    document.querySelector(".losses span").innerText = numberOfLoses;
    document.querySelector(".draws span").innerText = numberOfDraws;
}
//koncowe ustawienia i reset
function endGame(el) {
    document.querySelector(`[data-option="${el}"]`).style.boxShadow = "";
}

function startGame() {
    if (!game.playerHand) {
        return alert('Wybierz dłoń aby zagrać')
    };
    game.aiHand = aiChoice();
    const result = checkResult(game.playerHand, game.aiHand);
    console.log(result);
    publishResult(result);
    endGame(game.playerHand);
    game.playerHand = "";

}

// handSelection = (e) => {
//     console.log(e.target);
//     console.log(e.currentTarget);
// }
hands.forEach(hand => hand.addEventListener('click', handSelection));
document.querySelector(".start").addEventListener("click", startGame);