// Zobacz gotowy projekt: https://websamuraj.pl/examples/js/projekt7/

const gameSummary = {
    numbers: 0,
    wins: 0,
    losses: 0,
    draws: 0
}

let numberOfGames = 0;
let numberOfWins = 0;
let numberOfLoses = 0;
let numberOfDraws = 0;
let napis = "";

const game = {
    playerHand: "",
    aiHand: "",

}


const obrazki = [...document.querySelectorAll("img")];

function aiChoice() {
    if (game.playerHand)
        return game.aiHand = obrazki[Math.floor(Math.random() * 3)].getAttribute("data-option");
}

//sprawdzenie wartosci użytkownika i ai

function sprawdzWybor(player, ai) {
    if (!game.playerHand) alert("wybierz swoja dlon")
    else {

        if (player === ai) {
            return `draw`
        } else if (player === "kamień" && ai === "nożyczki") {
            return `win`
        } else if (player === "nożyczki" && ai === "papier") {
            return `win`
        } else if (player === "papaier" && ai === "kamień") {
            return `win`
        } else return `loss`

    }
}

function publicResult(value) {
    if (game.playerHand) {
        if (value == "win") {
            ++numberOfWins;
            ++numberOfGames;
            napis = "wygrałeś :)"
        } else if (value == "loss") {
            ++numberOfLoses;
            ++numberOfGames;
            napis = "przegrałeś :("
        } else {
            ++numberOfDraws;
            ++numberOfGames;
            napis = "remis!!"
        }
    }
    //podmiana wartości napisów
    document.querySelector('[data-summary="your-choice"]').innerText = game.playerHand;
    document.querySelector(`[data-summary="ai-choice"]`).innerText = game.aiHand;
    document.querySelector(`[data-summary="who-win"]`).innerText = napis;
    document.querySelector(".numbers span").innerText = numberOfGames;
    document.querySelector(".wins span").innerText = numberOfWins;
    document.querySelector(".losses span").innerText = numberOfLoses;
    document.querySelector(".draws span").innerText = numberOfDraws;

}

function endGame() {
    document.querySelectorAll("img").forEach(obrazek => obrazek.style.boxShadow = "");
    game.aiHand = "";
    game.playerHand = "";
}

function playerChoice() {
    obrazki.forEach(obrazek => obrazek.style.boxShadow = "");
    game.playerHand = this.getAttribute("data-option");
    this.style.boxShadow = "0 0 0 8px green";
    console.log(game.playerHand);
}
obrazki.forEach(obrazek => obrazek.addEventListener("click", playerChoice));

function startGame() {
    aiChoice();
    let wynik = sprawdzWybor(game.playerHand, game.aiHand);
    publicResult(wynik);
    endGame();


}

const btn = document.querySelector(".start").addEventListener("click", startGame);