const btnStart = document.querySelector(".start");
const btnReset = document.querySelector(".reset");
const stoper = document.querySelector(".stoper");
let number = 0;
let active = false;
let idI;

const counter = () => {

    if (!active) {
        btnStart.textContent = "Stop";
        active = !active;
        idI = setInterval(start, 10)

    } else {
        btnStart.textContent = "Start";
        active = !active;
        clearInterval(idI);
    }
}
start = () => {
    number++;
    stoper.textContent = (number / 100).toFixed(2);

}

reset = () => {

    document.querySelector(".stoper").textContent = "---";
    number = 0;
    btnStart.textContent = "Start";
    active = false;
    clearInterval(idI);


}

btnStart.addEventListener("click", counter);
btnReset.addEventListener("click", reset)