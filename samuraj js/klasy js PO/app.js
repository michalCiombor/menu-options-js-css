'use strict'

class Animal {
    constructor(age, name) {
        this.age = age;
        this.name = name;
    }
    breathe() {
        console.log(`Zwierze ${this.name} oddycha`)
    }
}

const Pies = new Animal(34, "kartofelek");

class Mammal extends Animal {
    constructor(age, name, lifetime) {
        super(age, name);
        this.lifetime = lifetime;
    }
    millk() {
        console.log("ssaki pija mleko")
    }
}

const ssak = new Mammal(89, "kajtek", 102);

class Human extends Mammal {
    constructor(age, name, lifetime, language) {
        super(age, name, lifetime);
        this.language = language;
    }
    human() {
        console.log('ludzie mowia w roznych jezykach')
    }
}

const human = new Human(45, "krzysiek", 203, "polski");


class SalesPearson {
    constructor(mobile, email) {
        this.mobile = mobile;
        this.email = email;

    }
    showText() {
        return 'Kiedy nieodbieram to prawdopodobnie nie dam rady gadac cwoku, zadzwon pozniej! nara!';
    }
    showMobile() {
        return this.mobile;
    }
    showEmail() {
        return this.email;
    }
}
let flag = false;
const Kedzior = new SalesPearson("888 888 888", "kedzio@grupa-wolff.eu");

document.querySelector("#kedzior").addEventListener("click", function () {
    this.textContent = Kedzior.showMobile();
})

document.querySelector("[data-set='popup']").addEventListener("click", function () {
    document.querySelector("[data-set='tekstPopup']").textContent = Kedzior.showText();
    document.querySelector("[data-set='tekstPopup']").classList.toggle("active");

    if (document.querySelector("[data-set='tekstPopup'").classList.contains("active")) {
        flag = false;
    } else flag = true;

    if (flag = !false) {
        document.body.style.backgroundColor = "gray";
    } else if (flag = false) {
        document.body.style.backgroundColor = "white";
    }
})