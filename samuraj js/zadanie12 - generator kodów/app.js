const chars = 'ABCDLMN0123456789';
const btn = document.querySelector("button");
const section = document.createElement("section");
section.style.position = "absolute";
section.style.top = "30vh";
document.body.appendChild(section);
const numberOfCodes = 10;
const numberOfCharacters = 8;
btn.textContent = `Wygeneruj ${numberOfCodes} kodów`
let nrKodu = "1";

generateCodes = () => {
    for (let i = 0; i < numberOfCodes; i++) {
        codes = "";

        for (let j = 0; j < numberOfCharacters; j++) {

            index = Math.floor(Math.random() * chars.length);
            codes += chars[index];
        }
        let div = document.createElement("div");
        div.textContent = `KOD ${nrKodu}: ${codes}`;
        section.appendChild(div);
        nrKodu++;


    }

}

btn.addEventListener("click", generateCodes);