document.body.addEventListener("click", (e) => {
    let x = e.clientX.toFixed(0);
    let y = e.clientY.toFixed(0);
    let color = getColor(e);

    console.log(x, y);
    document.body.style.backgroundColor = color;
})
getColor = (e) => {
    if (e.clientX % 2 == 0 && e.clientY % 2 == 0) {
        return "green";
    } else if (e.clientX % 2 == 0 || e.clientY % 2 == 0) {
        return "blue";
    } else if (e.clientX % 2 != 0 && e.clientY % 2 != 0) {
        return "red";
    }
}