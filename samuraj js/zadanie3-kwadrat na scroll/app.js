let height = 0;
let grow = true;
let windowHeight = window.innerHeight;
let square = document.createElement("div");
square.setAttribute("class", "square");
document.body.appendChild(square);
let currentHeight = window.scrollY;
// let square = document.querySelector(".square");

window.addEventListener("scroll", () => {

    if (grow == true) {
        height += 5;

        square.style.height = `${height}px`;
        square.style.width = `${height}px`;
    } else {
        height -= 5;

        square.style.height = `${height}px`;
        square.style.width = `${height}px`;
    }
    if (height >= windowHeight * 0.5) {
        grow = !grow;
    } else if (height <= 0) {
        grow = !grow;
    }





})