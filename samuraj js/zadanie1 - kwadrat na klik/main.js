let squareNumber = 0;

document.querySelector(".button").addEventListener("click", () => {
    squareNumber++;

    const showElements = document.querySelector(".showElements");
    let newSquare = document.createElement("div");
    (squareNumber % 5 !== 0) ? newSquare.classList.add("square"): newSquare.classList.add("circle");

    newSquare.textContent = squareNumber;
    showElements.appendChild(newSquare);



})

// document.querySelectorAll("div").forEach(el => el.addEventListener("click", () => this.classList.toggle("circle")));