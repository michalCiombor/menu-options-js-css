document.body.style.height = "100vh";
document.body.addEventListener("mousemove", function (e) {
    let x = e.clientX;
    let y = e.clientY;
    let height = window.innerHeight;
    let width = window.innerWidth;

    let red = (x / width) * 100;
    let green = (y / height) * 100;
    let blue = ((x / width * 100) + (y / height * 100)) / 2;
    document.body.style.backgroundColor = `rgb(${red}%,${green}%,${blue}%)`;
    document.querySelector("h1").textContent = `kolor: rgb(${red.toFixed(0)}%,${green.toFixed(0)}%,${blue.toFixed(0)}%)`;

})
let input = document.createElement("input");
input.setAttribute("placeholder", "kliknij aby dodać kolor");
input.style.fontSize = "20px";
input.style.position = "absolute";
input.style.left = 0;
input.style.bottom = 0;
input.style.transform = "translateY(-110%)"
input.style.zIndex = 10;
document.body.appendChild(input);
document.addEventListener("dblclick", function (event) {
    let inputValue = document.querySelector("h1").innerText;
    let newValue = inputValue.replace("KOLOR: ", "");

    input.setAttribute("value", newValue);
    return newValue
})
// document.querySelector("input").addEventListener("click", function (e) {
//     let copied = copy(e.target);
// })