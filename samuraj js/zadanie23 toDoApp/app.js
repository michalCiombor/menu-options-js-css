let insertTask = document.querySelector(".insertTask");
const addTask = document.querySelector("input[data-option='addTask']");
const tasksList = document.querySelector("ul[data-option='tasksList']");
const tasksArray = [];
let btnRemove = document.querySelectorAll("button");
const addTaskFn = (e) => {
    e.preventDefault();
    if (insertTask.value === "") return alert(`puste pole!`);
    let value = insertTask.value.toLowerCase();
    let task = document.createElement("li");

    task.innerHTML = `${value}  <BUTTON>USUŃ</button>`;
    tasksArray.push(task);
    // tasksList.appendChild(task);
    insertTask.value = "";
    renderList();

    tasksList.appendChild(task);

    document.querySelectorAll("button").forEach(el => el.addEventListener("click", (e) => {
        // e.target.parentNode.remove();
        const index = e.target.parentNode.dataset.key;
        tasksArray.splice(index, 1);
        renderList();

    }))
}

const renderList = () => {
    tasksList.textContent = "";
    tasksArray.forEach((toDoElement, key) => {
        toDoElement.dataset.key = key;
        tasksList.appendChild(toDoElement);
    })
}
const searchItems = () => {
    results = tasksArray.filter(li => li.textContent.toLowerCase().includes(insertTask));


    // tasksList.textContent = "";
    results.forEach(el => tasksList.appendChild(el));
}
addTask.addEventListener("click", addTaskFn);
insertTask.addEventListener("input", searchItems);