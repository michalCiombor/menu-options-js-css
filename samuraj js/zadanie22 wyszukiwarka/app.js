//definicja elementow

const input = document.querySelector("[data-type=searchValue]");
const liList = document.querySelectorAll("li");
const ul = document.querySelector("ul");

//funkcja wyszukiwania po tablicy
const search = (e) => {
    const searchText = e.target.value.toLowerCase();

    let liArray = [...liList];
    let results = liArray;
    results = results.filter(li => li.textContent.toLowerCase().includes(searchText));
    // console.log(results);

    ul.textContent = "";
    results.forEach(el => ul.appendChild(el))
}
input.addEventListener("input", search);